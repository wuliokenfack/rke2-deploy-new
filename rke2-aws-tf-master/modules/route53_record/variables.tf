variable "hosted_zone" {
  type = string
  description = "hosted zone"
}

variable "hostname" {
  type = string
  description = "hostname"
}

variable "private" {
  type = bool
  description = "whether or not this is a private zone"
  default = false
}

variable "type" {
  type = string
  description = "type"
}

variable "to_hostname" {
  type = string
  description = "host to route traffic to"
}