data "aws_route53_zone" "zone" {
  name         = var.hosted_zone
  private_zone = var.private
}

resource "aws_route53_record" "record" {
  zone_id = data.aws_route53_zone.zone.id
  name    = var.hostname
  type    = var.type
  ttl     = "300"
  records = [var.to_hostname]
}
