variable "env" {
  type = string
  description = "env"
}
variable "agent_role_arn" {
  type = string
  description = "agent role arn"
}

variable "agent_role_name" {
  type = string
  description = "agent role arn"
}

variable "storage_agent_role_arn" {
  type = string
  description = "storage agent role arn"
}