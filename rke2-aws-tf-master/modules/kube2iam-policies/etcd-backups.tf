# resource "aws_iam_role" "etcd_backups_role" {
#   name = "kube2iam-etcd-backups-${var.env}"

#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "sts:AssumeRole",
#       "Effect": "Allow",
#       "Principal": {
#         "Service": "ec2.amazonaws.com"
#       }
#     },
#     {
#       "Action": "sts:AssumeRole",
#       "Effect": "Allow",
#       "Principal": {
#         "AWS": ["${var.agent_role_arn}", "${var.storage_agent_role_arn}"]
#       }
#     }
#   ]
# }
# EOF
# }

# resource "aws_iam_policy" "etcd_backups_policy" {
#   name        = "kube2iam-etcd-backups-policy-${var.env}"
#   description = "Cluster autoscaler policy"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Action": "s3:*",
#       "Resource": [
#         "arn:aws:s3:::${var.s3_backups_bucket}/*",
#         "arn:aws:s3:::${var.s3_backups_bucket}"
#       ]
#     },
#   ]
# }
# EOF
# }

# resource "aws_iam_policy_attachment" "cluster-attach" {
#   name       = "etcd-backups-attachment-${var.env}"
#   roles      = [aws_iam_role.etcd_backups_role.name]
#   policy_arn = aws_iam_policy.etcd_backups_policy.arn
# }

