resource "aws_iam_role" "cluster_autoscaler_role" {
  name = "kube2iam-cluster-autoscaler-${var.env}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    },
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "AWS": ["${var.agent_role_arn}", "${var.storage_agent_role_arn}"]
      }
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cluster_autoscaler_policy" {
  name        = "kube2iam-cluster-autoscaler-policy-${var.env}"
  description = "Cluster autoscaler policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeAutoScalingInstances",
        "autoscaling:DescribeLaunchConfigurations",
        "autoscaling:SetDesiredCapacity",
        "autoscaling:TerminateInstanceInAutoScalingGroup",
        "autoscaling:DescribeLaunchConfigurations",
        "ec2:DescribeLaunchTemplateVersions",
        "autoscaling:DescribeTags"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "cluster-attach" {
  name       = "cluster-autoscaler-attachment-${var.env}"
  roles      = [aws_iam_role.cluster_autoscaler_role.name]
  policy_arn = aws_iam_policy.cluster_autoscaler_policy.arn
}

