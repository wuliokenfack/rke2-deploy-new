resource "aws_iam_role" "fluentd_role" {
  name = "kube2iam-fluentd-${var.env}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    },
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "AWS": ["${var.agent_role_arn}", "${var.storage_agent_role_arn}"]
      }
    }
  ]
}
EOF
}

resource "aws_iam_policy" "fluentd_policy" {
  name        = "kube2iam-fluentd-policy-${var.env}"
  description = "fluentd policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "fluentd-attach" {
  name       = "fluentd-attachment-${var.env}"
  roles      = [aws_iam_role.fluentd_role.name]
  policy_arn = aws_iam_policy.fluentd_policy.arn
}

