output "cluster_autoscaler_role" {
  value = aws_iam_role.cluster_autoscaler_role.name
}

output "cert_manager_role" {
  value = aws_iam_role.cert_manager_role.arn
}

output "fluentd_role" {
  value = aws_iam_role.fluentd_role.arn
}