resource "aws_iam_role_policy" "kube2iam-assume-roles" {
  name   = "kube2iam-assume-roles-${var.env}"
  role   = var.agent_role_name
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Resource": [
                "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/kube2iam-*",
                "${aws_iam_role.cert_manager_role.arn}"
            ]
        }
    ]
}
EOF
}