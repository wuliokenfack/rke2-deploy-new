
resource "aws_iam_role" "cert_manager_role" {
  provider = aws.route53
  name = "kube2iam-cert-manager-${var.env}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    },
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "AWS": ["${var.agent_role_arn}", "${var.storage_agent_role_arn}"]
      }
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cert_manager_policy" {
  provider = aws.route53
  name        = "kube2iam-cert-manager-policy-${var.env}"
  description = "Cert manager policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "route53:GetChange",
      "Resource": "arn:aws:route53:::change/*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ChangeResourceRecordSets",
        "route53:ListResourceRecordSets"
      ],
      "Resource": "arn:aws:route53:::hostedzone/*"
    },
    {
      "Effect": "Allow",
      "Action": "route53:ListHostedZonesByName",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "cert-manager-attach" {
  provider = aws.route53
  name       = "cert-manager-attachment-${var.env}"
  roles      = [aws_iam_role.cert_manager_role.name]
  policy_arn = aws_iam_policy.cert_manager_policy.arn
}

