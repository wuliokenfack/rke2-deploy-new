variable "is_develop" {
  type = bool
  default = false
}

variable "cluster_name" {
  type = string
}

variable "min_workers" {
  type = number
}

variable "max_workers" {
  type = number
}

variable "aws_region" {
  type = string
}

variable "rancher_env" {
  type = string
}

variable "firewall_ip" {
   type = string
}

variable "cluster_domain" {
  type = string
}

variable "container_logs_loggroup" {
  type = string
}

variable "vpc_tags" {
  type = map(string)
}

variable "ami" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "hosted_zone" {
  type = string
}