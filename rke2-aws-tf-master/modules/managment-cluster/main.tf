locals {
  cluster_name            = var.cluster_name
  aws_region              = var.aws_region
  rancher_hostname        = var.rancher_hostname
  rancher_version         = var.rancher_version
  container_logs_loggroup = var.container_logs_loggroup
  tags                    = var.tags
  vpc_tags                = var.vpc_tags
  hosted_zone             = var.hosted_zone
  firewall_ip             = var.firewall_ip
}

data "aws_vpc" "main" {
  tags = local.vpc_tags                                          # Use tags of VPC and Subnet
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    SubnetType = "private"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["693703738260"] # Hardened AMI August release

  filter {
    name = "name"
    values = ["20201229_Ubuntu_2004_LTS_MKP_hardened"]
  }
}

# Key Pair
resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ssh_pem" {
  filename        = "${local.cluster_name}.pem"
  content         = tls_private_key.ssh.private_key_pem
  file_permission = "0600"
}

resource "aws_key_pair" "key_pair" {
  key_name_prefix = local.cluster_name
  public_key      = tls_private_key.ssh.public_key_openssh
}

resource "aws_secretsmanager_secret" "secret_key" {
  name = "${local.cluster_name}-cluster-ssh"
  recovery_window_in_days = 0
  description = "ssh key for ${local.cluster_name} cluster"
  tags = {
    Name = "${local.cluster_name}-key"
  }
}

resource "aws_secretsmanager_secret_version" "secret_key_value" {
  secret_id     = aws_secretsmanager_secret.secret_key.id
  secret_string = jsonencode({
    private_key = tls_private_key.ssh.private_key_pem
    public_key = tls_private_key.ssh.public_key_openssh
  })
}

#
# Server
#
module "rke2" {
  source = "../../"
  providers = {
    aws = aws
  }

  cluster_name = local.cluster_name
  vpc_id       = data.aws_vpc.main.id
  subnets      = tolist(data.aws_subnet_ids.private.ids)
  ami                   = data.aws_ami.ubuntu.image_id # Note: Multi OS is primarily for example purposes
  ssh_authorized_keys   = [tls_private_key.ssh.public_key_openssh]
  #instance_type         = "t3a.large"
  instance_type         = "m5a.large"
  controlplane_internal = true # Note this defaults to best practice of true, but is explicitly set to public for demo purposes
  servers               = 3

  # Enable AWS Cloud Controller Manager
  enable_ccm = true

  rke2_config = <<-EOT
node-label:
  - "name=server"
  - "os=ubuntu"
EOT

  tags = local.tags
}

#
# Generic agent pool
#
module "agents" {
  source = "../agent-nodepool"
  providers = {
    aws = aws
  }

  name    = "generic"
  vpc_id       = data.aws_vpc.main.id
  subnets      = tolist(data.aws_subnet_ids.private.ids)
  ami                 = data.aws_ami.ubuntu.image_id 
  ssh_authorized_keys = [tls_private_key.ssh.public_key_openssh]
  spot                = true
# disable generic agents, don't know why we need them
  #asg                 = { min : 3, max : 3}
  asg                 = { min : 0, max : 0}
  instance_type       = "m5a.2xlarge"

  # Enable AWS Cloud Controller Manager and Cluster Autoscaler
  enable_ccm        = true
  enable_autoscaler = true

  rke2_config = <<-EOT
node-label:
  - "name=generic"
  - "os=ubuntu"
EOT

  cluster_data = module.rke2.cluster_data
  tags = local.tags
}

provider "aws" {
  alias = "route53"
}


# For demonstration only, lock down ssh access in production
resource "aws_security_group_rule" "quickstart_ssh" {
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = module.rke2.cluster_data.cluster_sg
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

# Generic outputs as examples
output "rke2" {
  value = module.rke2
}

# Example method of fetching kubeconfig from state store, requires aws cli and bash locally
resource "null_resource" "kubeconfig2" {
  depends_on = [module.rke2]
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    working_dir = "${path.module}/../k8s"
    command     = "aws s3 cp ${module.rke2.kubeconfig_path} rke2.yaml"
  }
}

module "kube2iam_policies" {
  source = "../kube2iam-policies"
  providers = {
    aws = aws
    aws.route53 = aws.route53
  }
  env    = module.rke2.cluster_name
  agent_role_arn = module.agents.iam_role_arn
  agent_role_name = module.agents.iam_role
  storage_agent_role_arn = module.agents.iam_role_arn
}

# resource "null_resource" "k8s_replace_control_plane_nodes" {
#   depends_on = [rancher2_cluster.cluster]
#   triggers = {
#     date = timestamp()
#     # ami_change = data.aws_ami.ubuntu
#   }
#   provisioner "local-exec" {
#     command = "chmod +x replace_nodes.sh && ./replace_nodes.sh"

#     environment = {
#       AWS_REGION               = local.aws_region
#       UPDATING_WORKERS         = "false"
#       NODE_ASG                 = module.rke2.server_nodepool_id
#       ADMIN_TOKEN              = local.rancher_admin_token
#       CLUSTER_ID               = rancher2_cluster.cluster.id
#       PROJECT_ID               = rancher2_cluster_sync.cluster_sync.system_project_id
#       RANCHER_URL              = local.rancher_url
#     }
#   }
# }

# resource "null_resource" "k8s_replace_storage_worker_nodes" {
#   depends_on = [rancher2_cluster.cluster, null_resource.k8s_replace_control_plane_nodes]
#   triggers = {
#     date = timestamp()
#     # ami_change = data.aws_ami.ubuntu
#   }
#   provisioner "local-exec" {
#     command = "chmod +x replace_nodes.sh && ./replace_nodes.sh"

#     environment = {
#       AWS_REGION               = local.aws_region
#       UPDATING_WORKERS         = "false"
#       UPDATING_STORAGE_NODES   = "true"
#       ADMIN_TOKEN              = local.rancher_admin_token
#       NODE_ASG                 = module.storage_agents.nodepool_id
#       CLUSTER_ID               = rancher2_cluster.cluster.id
#       PROJECT_ID               = rancher2_cluster_sync.cluster_sync.system_project_id
#       RANCHER_URL              = local.rancher_url
#     }
#   }
# }


# deploy 
resource "null_resource" "deploy_k8s_resources" {
  depends_on = [module.rke2, null_resource.kubeconfig2]
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec" {
    working_dir = "${path.module}/../k8s"
    interpreter = ["bash", "-c"]
    command     = "bash rancher_deploy.sh"
    environment = {
      AWS_REGION               = local.aws_region
      RANCHER_VERSION          = local.rancher_version
      RANCHER_HOSTNAME         = local.rancher_hostname
      CLUSTER_NAME             = module.rke2.cluster_name
      CLUSTER_AUTOSCALER_ROLE  = module.kube2iam_policies.cluster_autoscaler_role
      CERT_MANAGER_ROLE        = module.kube2iam_policies.cert_manager_role
      FLUENTD_ROLE             = module.kube2iam_policies.fluentd_role
      FLUENTD_LOG_GROUP        = local.container_logs_loggroup
      INSTALL_EFS              = false
    }
  }
}

data "external" "ingress_lb_hostname" {
  depends_on = [null_resource.deploy_k8s_resources]
  program = ["sh", "${path.module}/../k8s/get_ingress_lb_hostname.sh"]
  query = {
      KUBECONFIG = "${path.module}/../k8s/rke2.yaml"
    }
}

module "route53_public" { 
  source                 = "../route53_record"
  providers = {
    aws = aws.route53
  }
  private                = false
  type                   = "A"
  hosted_zone            = local.hosted_zone
  hostname               = local.rancher_hostname
  to_hostname            = local.firewall_ip
}

module "route53_private" {
  source                 = "../route53_record"
  private                = true
  type                   = "CNAME"
  hosted_zone            = local.hosted_zone
  hostname               = local.rancher_hostname
  to_hostname            = data.external.ingress_lb_hostname.result["hostname"]
}

provider "aws" {
  alias = "vpn"
}

module "route53_vpn_private" {
  source                 = "../route53_record"
  providers = {
    aws = aws.vpn
  }
  private                = true
  type                   = "CNAME"
  hosted_zone            = local.hosted_zone
  hostname               = local.rancher_hostname
  to_hostname            = data.external.ingress_lb_hostname.result["hostname"]
}

