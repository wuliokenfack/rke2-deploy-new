variable "cluster_name" {
   type = string 
}

variable "aws_region" {
   type = string 
}

variable "rancher_hostname" {
   type = string 
}

variable "hosted_zone" {
  type = string
}

variable "rancher_version" {
   type = string 
}

variable "firewall_ip" {
   type = string
}

variable "container_logs_loggroup" {
    type = string
}

variable "tags" {
    type = map(string)
}

variable "vpc_tags" {
  type = map(string)
}

variable "ami" {
  type = string
}
