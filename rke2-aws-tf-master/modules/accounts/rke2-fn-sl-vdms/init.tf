provider "rancher2" {
  alias     = "bootstrap"
  api_url   = "https://${local.rancher_hostname}"
  bootstrap = true
  insecure  = true
}

resource "null_resource" "wait_for_rancher" {
  count = terraform.workspace == local.management_cluster.name ? 1 : 0
  provisioner "local-exec" {
    command = <<EOF
# while [ "$${subject}" != "*  subject: CN=$${RANCHER_HOSTNAME}" ]; do
#     subject=$(curl -vk -m 2 "https://$${RANCHER_HOSTNAME}/ping" 2>&1 | grep "subject:")
#     echo "Cert Subject Response: $${subject}"
#     if [ "$${subject}" != "*  subject: O=cert-manager; CN=$${RANCHER_HOSTNAME}" ]; then
#       sleep 10
#     fi
# done
while [ "$${resp}" != "pong" ]; do
    resp=$(curl -sSk -m 2 "https://$${RANCHER_HOSTNAME}/ping")
    echo "Rancher Response: $${resp}"
    if [ "$${resp}" != "pong" ]; then
      sleep 10
    fi
done
EOF

    environment = {
      RANCHER_HOSTNAME = local.rancher_hostname
    }
  }
  depends_on = [
    module.management-cluster
  ]
}

resource "random_password" "admin_password" {
  count = terraform.workspace == local.management_cluster.name ? 1 : 0
  length = 20
  special = true
}

resource "rancher2_bootstrap" "admin" {
  count = terraform.workspace == local.management_cluster.name ? 1 : 0
  provider   = rancher2.bootstrap
  password   = random_password.admin_password.0.result
  depends_on = [null_resource.wait_for_rancher]
}

resource "aws_secretsmanager_secret" "rancher_admin_pass" {
  count = terraform.workspace == local.management_cluster.name ? 1 : 0
  name = "${local.rancher_env}-rancher-credentials"
  recovery_window_in_days = 0
  description = "rancher admin password for ${local.rancher_env}"
}

resource "aws_secretsmanager_secret_version" "rancher_admin_pass" {
  count = terraform.workspace == local.management_cluster.name ? 1 : 0
  secret_id     = aws_secretsmanager_secret.rancher_admin_pass.0.id
  secret_string = jsonencode({
    url = local.rancher_hostname
    admin_username = "admin"
    admin_password = random_password.admin_password.0.result
    admin_token = rancher2_bootstrap.admin[0].token
  })
}

data "aws_secretsmanager_secret" "rancher" {
  depends_on = [aws_secretsmanager_secret_version.rancher_admin_pass]
  # count      = local.management_cluster ? 0 : 1
  name = "${local.rancher_env}-rancher-credentials"
}

data "aws_secretsmanager_secret_version" "rancher" {
  # count      = local.management_cluster ? 0 : 1
  secret_id = data.aws_secretsmanager_secret.rancher.id
}
