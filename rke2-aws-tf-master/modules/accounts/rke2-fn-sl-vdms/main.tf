terraform {
 backend "s3" {
  encrypt = true
  #bucket = "fn-sl-vdms-tf-state"
  bucket = "rke2-fn-sl-vdms-tf-state"
  key = "state/vdms-rke2-test-mgmt"
#  dynamodb_table = "rke2-fn-sl-vdms-tf-state-lock"
  region = "us-east-1"
 }
}

locals {
  management_cluster = {
    "name"        = "vdms-rke2-test-mgmt"      #terraform workspace 
    "domain"      = "rke2.jcce.cloud"
    "firewall_ip" = "54.205.146.75"
  } 

  worker_clusters = {
    "vdms-rke2-testworker" = {
      "name"        = "vdms-rke2-test-worker" #terraform workspace 
      "min_workers" = 0
      "max_workers" = 0
      "domain"      = "dev.jcce.cloud"
      "firewall_ip" = "34.239.188.33"
   }
  }

  rancher_hostname      = local.management_cluster.domain
  hosted_zone = "jcce.cloud"
  aws_region   = "us-east-1"
  ami = "20210505_Ubuntu2004_MKP_hardened"
  rancher_env = "vdms-rke2-test-mgmt"                 # Name of the management cluster
  rancher_url  = "https://${jsondecode(data.aws_secretsmanager_secret_version.rancher.secret_string)["url"]}"
  rancher_admin_token = jsondecode(data.aws_secretsmanager_secret_version.rancher.secret_string)["admin_token"]
  route53_aws_profile = "FN-SL-VDMS"
  vpn_aws_profile = "FN-SL-VDSS"
  rancher_version      = "2.5.6"
  container_logs_loggroup = "testgroup"

  vpc_tags = {  # copy tags to identify vpc and subnet from aws console
    "Environment" = "vdms_rancher_mgmt"
    "Name" = "FN-SL-VDMS-K8S-VPC"
  }

  tags = {
    "terraform" = "true",
    "env"       = "vdms-rke2-test-cluster",
  }

}

provider "rancher2" {
  api_url  = local.rancher_url
  token_key = local.rancher_admin_token
  insecure = true
}

provider "aws" {
  alias   = "route53"
  region  = local.aws_region
  profile = local.route53_aws_profile
}

provider "aws" {
  alias   = "vpn"
  region  = local.aws_region
  profile = local.vpn_aws_profile
}

provider "aws" {
  region = local.aws_region
}

module "management-cluster" {
    count = terraform.workspace == local.management_cluster.name ? 1 : 0
    source = "../../managment-cluster"
    providers = {
        aws.route53         = aws.route53
        aws.vpn             = aws.vpn
        aws                 = aws
        rancher2            = rancher2
    }
    cluster_name = local.management_cluster.name
    aws_region = local.aws_region
    vpc_tags    = local.vpc_tags
    hosted_zone = local.hosted_zone
    ami = local.ami
    rancher_hostname = local.rancher_hostname
    firewall_ip      = local.management_cluster.firewall_ip
    rancher_version = local.rancher_version
    container_logs_loggroup = local.container_logs_loggroup
    tags = local.tags
}

 module "worker-cluster" {
    count = terraform.workspace == local.management_cluster.name ? 0 : 1
    source = "../../worker-cluster"
    providers = {
       aws.route53         = aws.route53
       aws.vpn             = aws.vpn
       aws                 = aws
       rancher2            = rancher2
    }

    is_develop = true
    cluster_name = terraform.workspace == local.management_cluster.name ? "" : local.worker_clusters[terraform.workspace].name #to prevent index error when deploying management cluster
    cluster_domain = terraform.workspace == local.management_cluster.name ? "" : local.worker_clusters[terraform.workspace].domain #to prevent index error when deploying management cluster
    firewall_ip = terraform.workspace == local.management_cluster.name ? "" : local.worker_clusters[terraform.workspace].firewall_ip #to prevent index error when deploying management cluster
    min_workers = terraform.workspace == local.management_cluster.name ? 0 : local.worker_clusters[terraform.workspace].min_workers #to prevent index error when deploying management cluster
    max_workers = terraform.workspace == local.management_cluster.name ? 0 : local.worker_clusters[terraform.workspace].max_workers #to prevent index error when deploying management cluster
    hosted_zone = local.hosted_zone
    vpc_tags    = local.vpc_tags
    ami = local.ami
    aws_region = local.aws_region
    rancher_env = local.rancher_env
    container_logs_loggroup = local.container_logs_loggroup
    tags = local.tags
}
