#!/bin/bash

set -e
input=$(jq -r '.')
for query_value in $(echo "${input}" | jq -r 'to_entries|map("\(.key)=\(.value|tostring)")|.[]'); do
  export ${query_value}
done


HOSTNAME=$(kubectl get svc nginx-ingress-controller -n nginx-ingress -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}')


jq -n --arg hostname "$HOSTNAME" '{"hostname":$hostname}'
